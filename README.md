The resources in this repository, which are from version 0.33.0 of Minecraft Earth, belong to Mojang. They are offered here to preserve them after the game's discontinuation on June 30, 2021.

The owner of this repository makes no claims of ownership over these files.
